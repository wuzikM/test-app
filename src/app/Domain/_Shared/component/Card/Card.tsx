import React from "react";
import styles from "./card.module.scss";

interface ICard {
  img: string;
  name: string;
}
export const Card = ({ img, name }: ICard) => {
  return (
    <div className={styles.card}>
      <img src={img} alt="img" />
      <div className={styles.shadow}>
        <div className={styles.name}>{name}</div>
      </div>
    </div>
  );
};
