import classNames from "classnames";
import React from "react";
import "./badget.scss";

interface Ibadget {
  size?: "xs" | "sm" | "md";
  children?: React.ReactNode;
  outlined?: boolean;
}
export const Badget = ({ size = "md", children, outlined }: Ibadget) => {
  const baseClass = "badget";
  const classes = classNames(
    baseClass,

    {
      [`${baseClass}--size-${size}`]: !!size,
    },
    {
      [`${baseClass}--outlined`]: !!outlined,
    }
  );
  return <div className={classes}>{children}</div>;
};
