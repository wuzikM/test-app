export interface IUserState {
  firstName: string;
  lastName: string;
  id: string;
}
