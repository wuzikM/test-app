const express = require("express");
const app = express();
const cors = require("cors");
const faker = require("faker");
const LoremIpsum = require("lorem-ipsum").LoremIpsum;
const port = 8088;

function randomNumber(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4,
  },
  wordsPerSentence: {
    max: 16,
    min: 4,
  },
});
const myId = "coolId";
const img2 =
  "https://s3-alpha-sig.figma.com/img/48e5/98ff/b83245fdcf42dfe0edb37a51fe8909e6?Expires=1634515200&Signature=KjjehIb0ArLmNo1Qhv9QQmqxTVPoyKIuVmOASPxFdFX~odOfzXvIQqsffDzCxtE9r~wmvy9IdOcG0n34FwuUIkk8dTtccTedmENdZuIx35LwuNbojyO1pU48mOThJ9tcblthHN31DvzHroZqqdBluauzf2cUnadzJDgUjRxaMKG-1pGMcrnWLilCQ24CKsZmkk26SR2CsCZLNAySLnKlCq7nAfK1FuE1meT~y5iGQu4B86t9DtVueYh1Gsdq4zx-4O2A51Nr~rH~TUO0KRr774ejy282a~BxXtnOVRO097jUxbUGh~jNYYjRxyq7Jhclm2PbcFMgnm6pUFEef~oHcQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA";

app.use(cors());
app.get("/api/user", (req, res) => {
  res.send({
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    id: myId,
  });
});

app.get("/api/favourites", (req, res) => {
  const favurites = [...Array(10).keys()].map((el) => ({
    name: `${faker.name.firstName()} ${faker.name.lastName()}`,
    img: "https://s3-alpha-sig.figma.com/img/48e5/98ff/b83245fdcf42dfe0edb37a51fe8909e6?Expires=1634515200&Signature=KjjehIb0ArLmNo1Qhv9QQmqxTVPoyKIuVmOASPxFdFX~odOfzXvIQqsffDzCxtE9r~wmvy9IdOcG0n34FwuUIkk8dTtccTedmENdZuIx35LwuNbojyO1pU48mOThJ9tcblthHN31DvzHroZqqdBluauzf2cUnadzJDgUjRxaMKG-1pGMcrnWLilCQ24CKsZmkk26SR2CsCZLNAySLnKlCq7nAfK1FuE1meT~y5iGQu4B86t9DtVueYh1Gsdq4zx-4O2A51Nr~rH~TUO0KRr774ejy282a~BxXtnOVRO097jUxbUGh~jNYYjRxyq7Jhclm2PbcFMgnm6pUFEef~oHcQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
    id: faker.random.uuid(),
  }));
  res.send(favurites);
});

app.get("/api/chatshorcuts", (req, res) => {
  const fafourites = [...Array(10).keys()].map((el) => {
    const randomNumber = faker.random.number({ min: 1, max: 10 });

    return {
      lastMessage: faker.git.commitMessage(),
      date: faker.datatype.datetime(),
      newMesseges:
        randomNumber > 4 ? faker.random.number({ min: 1, max: 15 }) : 0,
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      img: "https://s3-alpha-sig.figma.com/img/049e/630c/4f71f5dabd2806cdf2419c05b72f278f?Expires=1634515200&Signature=F01YbdL6T3jZvyfaO1uVQdEoh34dM9xhRF5QHarFFLqkvrHv---yDyJgs072a1zaW-bNReAsIQHCWBIKin6XsuEmMBjMKtRlL432x0aQADOPTm~9RjbA7tsVyW6kX7hghBa75qlBHb5vePgc2jzsDRmgiugYFLgoghua5r-8Row~dtrZYhAH6f5kt0Xdoy1MgtR3JkGHx4a6NYtnky8qHQiJMrS82m28qEWHBs2tBuAEmEHva0v5lx~TVcSYKClrS0WtnsPGOIexC9vnUqBxoPrB7Zyk1Ly~Cu~N~Y0VVdS9BomUNA0Sop6mickMWQn0PyEJ-sO~3uAzjR-gOXbnEw__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA",
      id: faker.random.uuid(),
    };
  });

  res.send(fafourites);
});

app.get("/api/chat", (req, res) => {
  const id = faker.random.uuid();
  const users = [
    ...[...Array(6).keys()].map((el) => ({
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      img: img2,
      id: faker.random.uuid(),
    })),
    {
      name: `${faker.name.firstName()} ${faker.name.lastName()}`,
      id: myId,
      img: img2,
    },
  ];

  const messages = [...Array(randomNumber(3, 15)).keys()].map((el) => {
    const random = randomNumber(0, users.length - 1);

    return {
      userId: users[random].id,
      message: lorem.generateWords(randomNumber(3, 10)),
      date: faker.datatype.datetime(),
    };
  });

  const chat = {
    id,
    users,
    messages,
  };

  res.send(chat);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
