import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import { ChatRoom } from "./pages/ChatRoom/ChatRoom";
import { Chat } from "./pages/Chat/Chat";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Chat} />
        <Route path="/chatroom/:id" component={ChatRoom} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
