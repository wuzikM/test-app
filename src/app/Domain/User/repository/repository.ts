import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { IUserState } from "../support/types";

export const loadUser = createAsyncThunk("user/loadUser", async () => {
  const response = await axios.get<IUserState>(
    `${process.env.REACT_APP_BASE_URL}/api/user`
  );
  return response.data;
});
