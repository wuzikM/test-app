import React from "react";
import { Badget } from "../../../_Shared/component/Badget/Badget";
import { useAppSelector } from "../../../_Shared/support/hooks";
import { selectUser } from "../../store/store";
import styles from "./userName.module.css";

export const UserName = () => {
  const { firstName, lastName } = useAppSelector(selectUser);
  return (
    <div className={styles.wrap}>
      <Badget />
      <span className={styles.user}>
        {firstName} {lastName}
      </span>
    </div>
  );
};
