import React from "react";
import { Badget } from "../../../_Shared/component/Badget/Badget";
import styles from "./messageIncoming.module.scss";

interface IMessage {
  message: string;
  img: string;
}
export const MessageIncoming = ({ message, img }: IMessage) => {
  return (
    <div className={styles.message}>
      <div className={styles.icon}>
        <Badget size="xs">
          <img src={img} alt="message" />
        </Badget>
      </div>
      {message}
    </div>
  );
};
