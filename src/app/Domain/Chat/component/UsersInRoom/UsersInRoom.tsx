import React from "react";
import { Badget } from "../../../_Shared/component/Badget/Badget";
import { useAppSelector } from "../../../_Shared/support/hooks";
import { selectUsersInRoom } from "../../store/store";
import styles from "./usersInRoom.module.scss";

export const UsersInRoom = () => {
  const users = useAppSelector(selectUsersInRoom);

  return (
    <div className={styles.users}>
      {users.map((el) => (
        <div key={el.id} className={styles.user}>
          <Badget size="sm" outlined>
            <img src={el.img} alt={el.name} />
          </Badget>
        </div>
      ))}
    </div>
  );
};
