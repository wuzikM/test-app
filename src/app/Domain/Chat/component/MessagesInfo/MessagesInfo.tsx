import React from "react";
import { Badget } from "../../../_Shared/component/Badget/Badget";
import { IChatShorcuts } from "../../support/types";
import styles from "./messagesInfo.module.scss";
import moment from "moment";

export const MessagesInfo = ({
  name,
  date,
  newMesseges,
  img,
  lastMessage,
}: IChatShorcuts) => {
  return (
    <div className={styles.messagesInfo}>
      <div className={styles.badgets}>
        {newMesseges ? (
          <>
            <div>
              <Badget size="sm">+{newMesseges}</Badget>
            </div>
            <div className={styles.secondBadget}>
              <Badget size="sm">
                <img src={img} alt="alt" />
              </Badget>
            </div>
          </>
        ) : (
          <Badget size="md">
            <img src={img} alt="alt" />
          </Badget>
        )}
      </div>
      <div className={styles.wrapInfo}>
        <div className={styles.name}>{name}</div>
        <div className={styles.info}> {lastMessage}</div>
      </div>
      <div className={styles.date}>{moment(date).format("DD MMM")}</div>
    </div>
  );
};
