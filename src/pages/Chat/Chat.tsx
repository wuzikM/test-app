import React, { useState, useEffect } from "react";
import { Favourites } from "../../app/Domain/Chat/component/Favourites/Favourites";
import { UserName } from "../../app/Domain/User/component/UserName/UserName";
import { Button } from "../../app/Domain/_Shared/component/Button/Button";
import { Input } from "../../app/Domain/_Shared/component/Input/Input";
import styles from "./chat.module.css";
import Subs from "../../assets/Subtract.png";
import { MessagesInfo } from "../../app/Domain/Chat/component/MessagesInfo/MessagesInfo";
import { selectChatShorcuts } from "../../app/Domain/Chat/store/store";
import {
  useAppDispatch,
  useAppSelector,
} from "../../app/Domain/_Shared/support/hooks";
import { loadChatShorcuts } from "../../app/Domain/Chat/repository/repository";
import { Link } from "react-router-dom";
import { loadUser } from "../../app/Domain/User/repository/repository";

export const Chat = () => {
  const [inputValue, setInputValue] = useState("");

  const chatShorcuts = useAppSelector(selectChatShorcuts);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(loadUser());
    dispatch(loadChatShorcuts());
  }, []);

  const handleChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  const handleClickAdd = () => {
    console.log("click add person");
  };

  return (
    <>
      <div className={styles.userHead}>
        <UserName />
      </div>
      <div className={styles.wrap}>
        <div className={styles.searching}>
          <Input
            type="text"
            placeholder="Search"
            value={inputValue}
            onChange={handleChangeInput}
          />
          <div className={styles.search}>
            <Button color="disabled" size="sm" onClick={handleClickAdd}>
              <img src={Subs} alt="searchButton" />
            </Button>
          </div>
        </div>
        <div className={styles.add}>
          <Button color="primary" size="sm" onClick={handleClickAdd}>
            <span style={{ fontSize: 20 }}>+</span>
          </Button>
        </div>
      </div>
      <div className={styles.favourite}>
        <Favourites />
      </div>
      <div className={styles.messages}>
        {chatShorcuts.map((el) => (
          <Link to={`/chatroom/${el.id}`} key={el.id}>
            <MessagesInfo {...el} />
          </Link>
        ))}
      </div>
    </>
  );
};
