import React, { useEffect, useState } from "react";
import { TextArea } from "../../app/Domain/Chat/component/TextArea/TextArea";
import { Button } from "../../app/Domain/_Shared/component/Button/Button";
import styles from "./chatRoom.module.scss";
import Camera from "../../assets/camera.png";
import Union from "../../assets/Union.png";
import { useAppDispatch } from "../../app/Domain/_Shared/support/hooks";
import { loadChat } from "../../app/Domain/Chat/repository/repository";
import { UsersInRoom } from "../../app/Domain/Chat/component/UsersInRoom/UsersInRoom";
import { Messages } from "../../app/Domain/Chat/component/Messages/Messages";
import { loadUser } from "../../app/Domain/User/repository/repository";
import { sendMessage } from "../../app/Domain/Chat/store/store";

export const ChatRoom = () => {
  const [message, setMessage] = useState("");
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(loadUser());
    dispatch(loadChat());
  }, []);

  const handleSendMessage = () => {
    dispatch(sendMessage(message));
    setMessage("");
  };

  return (
    <div className={styles.content}>
      <UsersInRoom />
      <div className={styles.messages}>
        <Messages />
      </div>
      <div className={styles.inputs}>
        <div className={styles.textarea}>
          <TextArea onChange={setMessage} value={message} />
        </div>
        <div className={styles.buttons}>
          <Button size="sm" color="success">
            <img src={Camera} alt="insert" />
          </Button>
          <Button size="sm" color="secondary" onClick={handleSendMessage}>
            <img src={Union} alt="message" />
          </Button>
        </div>
      </div>
    </div>
  );
};
