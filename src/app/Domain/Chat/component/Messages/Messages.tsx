import React, { useEffect, useRef } from "react";
import { selectUser } from "../../../User/store/store";
import { useAppSelector } from "../../../_Shared/support/hooks";
import { selectMessages, selectUsersInRoom } from "../../store/store";
import { MessageIncoming } from "../MessageIncoming/MessageIncoming";
import { MessageOutcoming } from "../MessageOutcoming/MessageOutoming";
import styles from "./messages.module.scss";

export const Messages = () => {
  const messages = useAppSelector(selectMessages);
  const users = useAppSelector(selectUsersInRoom);
  const { id } = useAppSelector(selectUser);

  const takeImg = (userId: string): string => {
    return users.find((el) => el.id === userId)?.img ?? "";
  };

  const isMyMessage = (userId: string): boolean => {
    return id === userId;
  };

  const messagesEndRef = useRef<HTMLDivElement>(null);
  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };
  useEffect(scrollToBottom, [messages]);

  return (
    <div className={styles.messages}>
      {messages.map((el, i) =>
        isMyMessage(el.userId) ? (
          <div className={styles.messageOutcoming} key={i}>
            <MessageOutcoming message={el.message} />
          </div>
        ) : (
          <div className={styles.messageIncoming} key={i}>
            <MessageIncoming message={el.message} img={takeImg(el.userId)} />
          </div>
        )
      )}
      <div ref={messagesEndRef} />
    </div>
  );
};
