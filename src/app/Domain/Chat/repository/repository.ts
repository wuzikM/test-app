import axios from "axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { IChat, IChatShorcuts, IFavouritePerson } from "../support/types";

export const loadFavourites = createAsyncThunk(
  "chat/loadFavourites",
  async () => {
    const response = await axios.get<IFavouritePerson[]>(
      `${process.env.REACT_APP_BASE_URL}/api/favourites`
    );
    return response.data;
  }
);

export const loadChatShorcuts = createAsyncThunk(
  "chat/loadChatShorcuts",
  async () => {
    const response = await axios.get<IChatShorcuts[]>(
      `${process.env.REACT_APP_BASE_URL}/api/chatshorcuts`
    );
    return response.data;
  }
);

export const loadChat = createAsyncThunk("chat/loadChat", async () => {
  const response = await axios.get<IChat>(
    `${process.env.REACT_APP_BASE_URL}/api/chat`
  );
  return response.data;
});
