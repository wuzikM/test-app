import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../_Shared/store/store";
import {
  loadChat,
  loadChatShorcuts,
  loadFavourites,
} from "../repository/repository";
import { IChatStore } from "../support/types";

const initialState: IChatStore = {
  favourites: [],
  chatShorcuts: [],
  chat: null,
};

export const chatSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    sendMessage: (state, action: PayloadAction<string>) => {
      if (state.chat) {
        state.chat.messages.push({
          message: action.payload,
          userId: "coolId",
          date: Date.now(),
        });
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loadFavourites.fulfilled, (state, action) => {
      state.favourites = action.payload;
    });
    builder.addCase(loadChatShorcuts.fulfilled, (state, action) => {
      state.chatShorcuts = action.payload;
    });
    builder.addCase(loadChat.fulfilled, (state, action) => {
      state.chat = action.payload;
    });
  },
});

export default chatSlice.reducer;

export const { sendMessage } = chatSlice.actions;

export const selectFavourites = (state: RootState) => state.chat.favourites;
export const selectChatShorcuts = (state: RootState) => state.chat.chatShorcuts;
export const selectChat = (state: RootState) => state.chat.chat;
export const selectUsersInRoom = (state: RootState) =>
  state.chat.chat?.users ?? [];
export const selectMessages = (state: RootState) =>
  state.chat.chat?.messages ?? [];
