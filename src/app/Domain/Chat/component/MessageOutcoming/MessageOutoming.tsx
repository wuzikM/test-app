import React from "react";
import styles from "./messageOutcoming.module.scss";

interface IMessage {
  message: string;
}
export const MessageOutcoming = ({ message }: IMessage) => {
  return <div className={styles.message}>{message}</div>;
};
