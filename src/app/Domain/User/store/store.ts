import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../_Shared/store/store";
import { loadUser } from "../repository/repository";
import { IUserState } from "../support/types";

const initialState: IUserState = {
  firstName: "",
  lastName: "",
  id: "",
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(loadUser.fulfilled, (state, action) => {
      state.firstName = action.payload.firstName;
      state.lastName = action.payload.lastName;
      state.id = action.payload.id;
    });
  },
});

export default userSlice.reducer;

export const selectUser = (state: RootState) => state.user;
