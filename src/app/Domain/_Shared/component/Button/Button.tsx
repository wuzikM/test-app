import classNames from "classnames";
import React from "react";
import "./button.scss";

interface IButton {
  color: "primary" | "success" | "secondary" | "disabled";
  size: "sm";
  children?: React.ReactNode;
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
}

export const Button = ({ color, size, children, onClick }: IButton) => {
  const baseClass = "button";
  const classes = classNames(
    baseClass,

    {
      [`${baseClass}--color-${color}`]: !!color,
    },
    {
      [`${baseClass}--size-${size}`]: !!size,
    }
  );

  return (
    <button className={classes} onClick={onClick}>
      {children}
    </button>
  );
};
