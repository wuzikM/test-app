import { Nullable } from "../../_Shared/support/types";

export interface IChatStore {
  favourites: IFavouritePerson[];
  chatShorcuts: IChatShorcuts[];
  chat: Nullable<IChat>;
}

export interface IFavouritePerson {
  name: string;
  img: string;
  id: string;
}

export interface IChatShorcuts {
  id: number;
  name: string;
  lastMessage: string;
  date: Date;
  img: string;
  newMesseges: number;
}

export interface IChat {
  id: string;
  users: {
    name: string;
    img: string;
    id: string;
  }[];
  messages: {
    userId: string;
    message: string;
    date: number;
  }[];
}
