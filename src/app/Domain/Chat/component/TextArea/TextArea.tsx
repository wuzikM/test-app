import React, { useRef, useEffect } from "react";
import styles from "./textArea.module.scss";

interface ITextArea {
  value: string;
  onChange: (value: string) => void;
}
export const TextArea = ({ value, onChange }: ITextArea) => {
  const ref = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (!value && ref.current) {
      ref.current.innerText = "";
    }
  }, [value]);

  return (
    <div
      ref={ref}
      contentEditable="true"
      className={styles.textarea}
      onInput={(e) => onChange(e.currentTarget.innerText)}
      suppressContentEditableWarning={true}
    ></div>
  );
};
