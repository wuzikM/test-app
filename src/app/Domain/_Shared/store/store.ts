import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import userReducer from "../../User/store/store";
import chatStore from "../../Chat/store/store";

export const store = configureStore({
  reducer: {
    user: userReducer,
    chat: chatStore,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
