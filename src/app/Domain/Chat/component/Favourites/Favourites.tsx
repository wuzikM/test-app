import React, { useEffect } from "react";
import SwiperCore, { Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import { Card } from "../../../_Shared/component/Card/Card";
import { useAppDispatch, useAppSelector } from "../../../_Shared/support/hooks";
import { loadFavourites } from "../../repository/repository";
import { selectFavourites } from "../../store/store";
import "swiper/swiper.scss";
import "swiper/swiper-bundle.min.css";
import "./favourites.scss";
SwiperCore.use([Pagination]);

export const Favourites = () => {
  const favourites = useAppSelector(selectFavourites);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(loadFavourites());
  }, []);

  return (
    <div>
      <div className="favourites">Favourites</div>
      <Swiper
        pagination={{ type: "bullets" }}
        spaceBetween={15}
        slidesPerView={3}
        onSlideChange={() => console.log("slide change")}
        onSwiper={(swiper) => console.log(swiper)}
      >
        {favourites.map((el) => (
          <SwiperSlide key={el.id}>
            <Card {...el} />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};
